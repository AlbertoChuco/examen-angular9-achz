import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicComponent } from './public/public.component';
import { CursosComponent } from './public/cursos/cursos.component';
import { RouterModule } from '@angular/router';
import { SharedComponentsModule } from '../shared/components/components.module';
import { PrivateModule } from '../modules/private/private.module';



@NgModule({
  declarations: [
     PublicComponent,
     CursosComponent
    ],
  imports: [
    CommonModule,
    RouterModule,
    SharedComponentsModule,
    PrivateModule
  ],
  exports:[
    PublicComponent,
    CursosComponent
  ]
})
export class CoreModule { }
