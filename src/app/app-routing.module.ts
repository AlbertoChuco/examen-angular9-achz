import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicComponent } from './core/public/public.component';
import { CursosComponent } from './core/public/cursos/cursos.component';


const routes: Routes = [
  {path: '',
   component: PublicComponent,
   children:[
     {path: '', component: CursosComponent},
   ]
  },
  {
    path: 'cursos',
    loadChildren: () => import('./modules/private/private.module').then(m => m.PrivateModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
