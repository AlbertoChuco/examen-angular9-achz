export interface Cursos {
    id: string,
    img: string,
    instructor: string,
    progress: string,
    title: string
}

export interface CursosDetail {
    classes: any[],
    section: string,
    title: string
}