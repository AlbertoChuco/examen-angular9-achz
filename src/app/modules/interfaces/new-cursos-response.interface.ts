import { Cursos } from './curso-response.interface';

export interface NewCursosReponse{
    cursos: Cursos[]
}