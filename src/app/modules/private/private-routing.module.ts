import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrivateComponent } from './private.component';
import { NewCursosComponent } from './pages/new-cursos/new-cursos.component';
import { CursosDetailComponent } from './pages/cursos-detail/cursos-detail.component';


const routes: Routes = [
  {path: '', component: PrivateComponent,
  children:[
    {path: '', component: NewCursosComponent},
    {path: 'cursos-detail/:id', component: CursosDetailComponent},
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrivateRoutingModule { }
