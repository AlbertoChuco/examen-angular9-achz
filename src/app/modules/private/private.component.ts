import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-private',
  templateUrl: './private.component.html',
  styleUrls: ['./private.component.scss']
})
export class PrivateComponent implements OnInit {
  header = {
    logo: 'assets/logo.png',
  }

  constructor() { }

  ngOnInit(): void {
  }

}
