import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CursosItemComponent } from './cursos-item/cursos-item.component';



@NgModule({
  declarations: [
    CursosItemComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    CursosItemComponent
  ]
})
export class ComponentsModule { }
