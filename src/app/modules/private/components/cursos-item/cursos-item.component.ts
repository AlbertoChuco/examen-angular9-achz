import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Cursos } from 'src/app/modules/interfaces/curso-response.interface';


@Component({
  selector: 'app-cursos-item',
  templateUrl: './cursos-item.component.html',
  styleUrls: ['./cursos-item.component.scss']
})
export class CursosItemComponent implements OnInit {
 
  @Input() id:string;
  @Input() img:string;
  @Input() instructor: string;
  @Input() progress: string;
  @Input() title: string;

  // @Input() cursos: Cursos[] = [];

  @Output() goCurso: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }


}
