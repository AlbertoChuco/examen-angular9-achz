import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrivateRoutingModule } from './private-routing.module';
import { PrivateComponent } from './private.component';
import { NewCursosComponent } from './pages/new-cursos/new-cursos.component';
import { CursosDetailComponent } from './pages/cursos-detail/cursos-detail.component';
import { SharedComponentsModule } from 'src/app/shared/components/components.module';
import { ComponentsModule } from './components/components.module';
import { RouterModule } from '@angular/router';
import { ServicesModule } from './services/services.module';


@NgModule({
  declarations: [
    PrivateComponent,
    NewCursosComponent,
    CursosDetailComponent
  ],
  imports: [
    CommonModule,
    PrivateRoutingModule,
    SharedComponentsModule,
    RouterModule,
    ComponentsModule,
    ServicesModule
  ]
})
export class PrivateModule { }
