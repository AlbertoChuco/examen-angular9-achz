import { Injectable } from '@angular/core';
import { ServicesModule } from './services.module';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Cursos, CursosDetail } from '../../interfaces/curso-response.interface';

@Injectable({
  providedIn: ServicesModule
})
export class GalaxyService {

  constructor(private http: HttpClient) { }
  getNewCursos(): Observable<Cursos[]>{
    return this.http.get<any[]>('https://ionicapp-7a398.firebaseio.com/galaxydemy/subjects.json');
  }

  getCursoDetail(id: string): Observable<CursosDetail>{
    return this.http.get<CursosDetail>(`https://ionicapp-7a398.firebaseio.com/galaxydemy/subject/${id}.json`);
  }
}
