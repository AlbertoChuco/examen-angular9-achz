import { Component, OnInit } from '@angular/core';
import { GalaxyService } from '../../services/galaxy.service';
import { CursosDetail } from 'src/app/modules/interfaces/curso-response.interface';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cursos-detail',
  templateUrl: './cursos-detail.component.html',
  styleUrls: ['./cursos-detail.component.scss']
})
export class CursosDetailComponent implements OnInit {
  detalle: CursosDetail;
  constructor(
    private galaxy: GalaxyService,
    private activatedRoute: ActivatedRoute
    ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    this.galaxy.getCursoDetail(id)
    .subscribe(
       detalle => this.detalle = detalle,
       err => console.log(err)

    )
  }

}
