import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCursosComponent } from './new-cursos.component';

describe('NewCursosComponent', () => {
  let component: NewCursosComponent;
  let fixture: ComponentFixture<NewCursosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCursosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCursosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
