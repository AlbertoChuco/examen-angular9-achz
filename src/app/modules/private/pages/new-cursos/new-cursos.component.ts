import { Component, OnInit } from '@angular/core';
import { GalaxyService } from '../../services/galaxy.service';
import { Cursos } from 'src/app/modules/interfaces/curso-response.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-cursos',
  templateUrl: './new-cursos.component.html',
  styleUrls: ['./new-cursos.component.scss']
})
export class NewCursosComponent implements OnInit {
   cursos: Cursos[]= [];
   

  constructor(
    private galaxy: GalaxyService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.galaxy.getNewCursos()
    .subscribe(
      cursos => this.cursos = cursos,
      err => console.log(err),
    )
  }

  goCurso(id: string){
    this.router.navigate(['/cursos/cursos-detail', id])
  //  this.router.navigateByUrl('')
  }

}
